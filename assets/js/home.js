  const slider = document.getElementsByClassName("slider")[0];
  const slides = [
    document.getElementById("slide1"),
    document.getElementById("slide2"),
    document.getElementById("slide3"),
    document.getElementById("slide4"),
    document.getElementById("slide5"),
  ];

  let autoplayInterval = setInterval(autoplay, 5000);

  slider.addEventListener("mouseover", () => {
    if (autoplayInterval) {
      clearInterval(autoplayInterval);
      autoplayInterval = null;
    }
  });
  slider.addEventListener("mouseout", () => {
    if (!autoplayInterval) {
      autoplayInterval = setInterval(autoplay, 5000);
    }
  });

  function autoplay() {
    let currentSlide;
    slides.forEach((slide, index) => {
      if (slide.checked) {
        currentSlide = index;
      }
    });
    slides[(currentSlide + 1) % slides.length].checked = true;
  }
