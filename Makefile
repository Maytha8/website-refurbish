#!/usr/bin/env make

all: build

build: generate-translations
	hugo

updatepo:
	json2po -P i18n/en.json po/templates.pot

generate-translations:
	po2json po/ i18n/ -t po/templates.json
	ln -s ../po/templates.json i18n/en.json

clean:
	rm -rf i18n/
	rm -rf public/
	rm -rf resources/

server: generate-translations
	while true; do \
		inotifywait -qre close_write po/; \
		$(MAKE) generate-translations; \
	done &
	hugo server

