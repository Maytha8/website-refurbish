const toml = require("toml");
const fs = require("node:fs");
const path = require("node:path");

module.exports = {
  plugins: [
    require("postcss-nested"),
    require("postcss-rtlcss"),
    require("postcss-url")({
      url: (asset) => {
        if (!asset.url.startsWith("/")) return asset.url;
        const baseURL = new URL(
          toml.parse(require("node:fs").readFileSync("hugo.toml"))["baseURL"],
        );
        const newPath = path.join(baseURL.pathname, asset.pathname);
        return newPath;
      },
    }),
    require("autoprefixer"),
  ],
};
