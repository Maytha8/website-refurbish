+++
title = "Getting Debian"
+++

Getting a copy of Debian is easy! Try one of the following options for
downloading Debian.

## Download an installation image

- A [small installation image](/download/netinst): can be downloaded quickly
  and should be recorded onto a removable disk. To use this, you will need a
  machine with an Internet connection.

- A larger [complete installation image](/CD): contains more packages, making
  it easier to install machines without an Internet connection.
